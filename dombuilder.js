DOMBuilder = function(doc) {
    this.doc = doc || document;
    this.frag = document.createDocumentFragment();
    this.pointer = this.frag;
}

DOMBuilder.prototype.addBox = function ( def, attrs ) {
    this.pointer = this.add( def, attrs );
    return this.pointer;
}

DOMBuilder.prototype.endBox = function ( ) {
    try{
        this.pointer = this.pointer.parentNode;
        return this.pointer;
    } catch(e){
        return null;
    }
}

DOMBuilder.prototype.addText = function ( txt ) {
    var elm = this.doc.createTextNode(txt);
    this.pointer.appendChild(elm);
    return elm;
}

DOMBuilder.prototype.add = function ( def, attrs ) {
    var elm = DOMBuilder.make( def, attrs||false, this.doc);
    this.pointer.appendChild(elm);
    return elm;
}

DOMBuilder.prototype.addElement = function ( elm ) {
    this.pointer.appendChild(elm);
    return elm;
}

DOMBuilder.prototype.addBoxElement = function ( elm ) {
    this.pointer.appendChild(elm);
    this.pointer = elm;
    return elm;
}

DOMBuilder.prototype.getDom = function ( ) {
    return this.frag;
}

DOMBuilder.prototype.appendTo = function ( elm ) {
    if( typeof(elm) == "string" ){
        var elm = document.getElementById(elm);
    }
    if( !elm ){
        throw new Error("DOMBuilder.appendTo: 'elm' not found.");
    }
    elm.appendChild(this.frag);
}

// tagName#id.class.class2
DOMBuilder.make = function(q,attrs,doc,nodecor){
    var attrs = attrs||false;
    var doc = doc || document;
    var def = DOMBuilder._queryparse(q);
    var elm = doc.createElement(def.tag);
    if(def.className.length) elm.className = def.className.join(" ");
    if(def.id) elm.id = def.id;
    if(attrs){
        for(var key in attrs){
            elm[key] = attrs[key];
        }
    }
    return elm;
}

DOMBuilder._queryparse = function(query){
    var ob = {id: "", tag: "", className: []};
    var e = "t";
    var ci = -1;
    for(var n = 0; n < query.length; n ++ ){
        var chr = query.charAt(n);
        if(chr == "#"){e="i"; continue;};
        if(chr == "."){e="c"; ci++; ob.className.push(""); continue;};
        if(e == "c") ob.className[ci] += chr;
        if(e == "t") ob.tag += chr;
        if(e == "i") ob.id += chr;
    }
    return ob;
}
